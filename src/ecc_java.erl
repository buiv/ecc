-module(ecc_java).

-export([insert/2, lookup/1, delete/1, list_keys/0]).

insert(Key, Value) ->
	%ecc:insert_auto_dep(Key, Value),    
    ecc:insert_simple(Key, Value),
    ok.

lookup(Key) ->
	%ecc:lookup(Key).
	ecc:lookup_simple(Key).

delete(Key) ->
    ecc_core:delete(Key).

list_keys() -> 
    ecc_core:list_keys().