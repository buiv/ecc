Sample command: 

1> application:start(ecc).
ok

2> ecc:insert(one, two).
ecc_core:<0.38.0>: EccCore: receive put/key/value call
ok

3> ecc:lookup(one).
ecc_core:<0.38.0>: EccCore: receive get/key call
ecc_core:<0.38.0>: EccCore: get data from riak
{ok,one,0,two}

4> ecc:list_keys().
ecc_core:<0.38.0>: EccCore: receive listkeys call
[{one}]

5> ecc:insert(four, five).
ecc_core:<0.38.0>: EccCore: receive put/key/value call
ok

6> ecc:list_keys().
ecc_core:<0.38.0>: EccCore: receive listkeys call
[{four},{one}]

7> ecc:delete(one).
ecc_core:<0.38.0>: EccCore: receive delete/key call
ecc_core:<0.38.0>: in delete data with key one in buckets
ok

5> ecc:delete_all_keys().
ecc_core:<0.44.0>: EccCore: receive delete all keys call
ecc_store:<0.44.0>: ecc_store: in delete data with key four in buckets
ecc_store:<0.44.0>: ecc_store: in delete data with key one in buckets
[ok,ok]

6> ecc:get_deplist().

2> ecc:get_key_version(vanroy).


2> ecc:insert(one, two, []).

2> ecc:insert(vanroy, prof, [{manuel, 0}]).


ecc:insert(vanroy, prof, [{manuel, 1}]).

8> ecc_core:put_remote(peter, 1, handsome, [{viet, 0}]).

9> ecc_core:put_remote(peter, 2, handsome, [{viet, 1}]).

10> ecc_core:put_remote(peter, 2, handsome, [{hang, 1}]).

11> ecc:get_buffer_list().

